# Konteiner eesti keele morfoloogilise ja süntaktilise analüüsi tegemiseks spaCy töövoona

SpaCy töövoogu sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).


## Mida sisaldab <a name="Mida_sisaldab"></a>

* [UD v2.5 eesti puudepangal](https://github.com/UniversalDependencies/UD_Estonian-EDT) treenitud ja keelemudelit [xml-roberta-base](https://huggingface.co/xlm-roberta-base) sisaldav [SpaCy töövoog eesti keele morfoloogiliseks ja süntaktiliseks märgendamiseks](https://github.com/EstSyntax/EstSpaCy); seejuures tehakse ka sõnestamine ja lausestamine; morfoloogilised kategooriad on [UD](https://universaldependencies.org/)-kohased. 
* Konteineri ja liidesega seotud lähtekood


## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/elg_estspacysuntax:2022.06.29
```
Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. [Lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git) allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git gitlab_docker_elg_estspacysuntax
```

### 2. Konteineri kokkupanemine 

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estspacysuntax
docker build -t tilluteenused/elg_estspacysuntax:2022.06.29 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/elg_estspacysuntax:2022.06.29
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* Analüüsitav tekst */
}
```

## Vastuse json-kuju

```json
{
  "response":{
    "type":"texts",
    "texts":[             /* töödeldud sõnede massiiv */
      {
        "content": string /* töödeldud sõne */
        "features":
            {
                "i": int,
                "is_sent_end": boolean, /* Kui lause lõpp True, muidu False */
                "is_sent_start": boolean, /* Kui lause algus True, muidu False */
                "pos": string, /* Sõnaliik */
                "morph": string, /* Morfoloogiline info */
                "head": int,
                "dep": string, /* {"ROOT"|"obj"|"punct"|"prt"|...}
            }
      }
    ]
  }
}
```
## Kasutusnäide

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text", "content":"Mees peeti kinni. Sarved&Sõrad"}' http://127.0.0.1:5000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Wed, 29 Jun 2022 14:52:17 GMT
Connection: close
Content-Type: application/json
Content-Length: 760

{"response":{"type":"texts","texts":[{"content":"Mees","features":{"i":0,"is_sent_end":false,"is_sent_start":true,"pos":92,"morph":"Case=Nom|Number=Sing","head":"1","dep":"obj"}},{"content":"peeti","features":{"i":1,"is_sent_end":false,"is_sent_start":false,"pos":100,"morph":"Mood=Ind|Tense=Past|VerbForm=Fin|Voice=Pass","head":"1","dep":"ROOT"}},{"content":"kinni","features":{"i":2,"is_sent_end":false,"is_sent_start":false,"pos":86,"morph":"","head":"1","dep":"compound:prt"}},{"content":".","features":{"i":3,"is_sent_end":true,"is_sent_start":false,"pos":97,"morph":"","head":"1","dep":"punct"}},{"content":"Sarved&S\u00f5rad","features":{"i":4,"is_sent_end":true,"is_sent_start":true,"pos":96,"morph":"Case=Nom|Number=Sing","head":"4","dep":"ROOT"}}]}}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep 

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
