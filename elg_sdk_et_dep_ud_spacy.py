#!/usr/bin/env python3

import json
import sys
import os
os.environ["PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION"] = "python"

from typing import Dict, List

from elg import FlaskService
from elg.model import TextsResponse, TextRequest

class PIPELINE:
    """Load and run pipeline
    """
    def __init__(self, pl:str) -> None:
        """Load pipeline

        Args:
            pl (str): pipelane name {'et_dep_ud_sm'|'et_dep_ud_estbert'|'et_dep_ud_xlmroberta'}
        """
        self.pipeline_name = pl
        self.pipeline = None

    def doit(self, text:str) -> List:
        """Run pipeline

        Args:
            text (str): input text

        Returns:
            List: output from pipeline
        """
        if self.pipeline == None:
            #print("== Initsialiseerime:", self.pipeline_name)
            try:
                self.pipeline = __import__(self.pipeline_name).load()
            except ImportError:
                # Display error message
                print("Cant import pipeline:", self.pipeline_name, file=sys.stderr)
                return None

        doc = self.pipeline(text)
        texts_out = []
        for sentence in doc.sents:
            for word in sentence:
                txt = {}
                txt["content"] = str(word)
                txt["features"] = {
                    "i": word.i, 
                    "is_sent_end": word.is_sent_end,
                    "is_sent_start": word.is_sent_start,
                    "pos": word.pos, 
                    "morph": str(word.morph), 
                    "head": str(word.head.i),
                    "dep": word.dep_}
                texts_out.append(txt)
        return texts_out

#et_dep_ud_sm = PIPELINE('et_dep_ud_sm')        
#et_dep_ud_estbert = PIPELINE('et_dep_ud_estbert')
et_dep_ud_xlmroberta = PIPELINE('et_dep_ud_xlmroberta')

       
class ESTSPACY(FlaskService):
    def process_text(self, request) -> TextsResponse:
        #if request.params["pipeline"] == 'sm':
        #    texts_out = et_dep_ud_sm.doit(request.content)
        #elif request.params["pipeline"] == 'estbert':
        #    texts_out = et_dep_ud_estbert.doit(request.content)
        #elif request.params["pipeline"] == 'xlmroberta':
        #    texts_out = et_dep_ud_xlmroberta.doit(request.content)
        #else:
        #    texts_out = [] # wrong workflow name in params
        texts_out = et_dep_ud_xlmroberta.doit(request.content)
        return TextsResponse(texts=texts_out)
        
flask_service = ESTSPACY("EstSPACY")
app = flask_service.app

def run_test(my_query_str: str) -> None:
    my_query = json.loads(my_query_str)
    service = ESTSPACY("EstSPACY")
    request = TextRequest(content=my_query["content"], params=my_query["params"])
    response = service.process_text(request)

    response_str = response.json(exclude_unset=True)  # exclude_none=True
    response_json = json.loads(response_str)
    return response_json

def run_server() -> None:
    app.run()

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='sentences to precrocess')
    args = argparser.parse_args()
    if args.json is None:
        run_server()
    else:
        json.dump(run_test(args.json), sys.stdout, indent=4)




