# Container of a spaCy pipeline for Estonian morphological and syntactic analysis 

A spaCy pipeline container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).


## Contains  <a name="Contains"></a>

* [SpaCy pipeline for morphological and syntactic analysis of Estonian](https://github.com/EstSyntax/EstSpaCy), trained on 
[UD v2.5 Estonian treebank](https://github.com/UniversalDependencies/UD_Estonian-EDT) and using [xml-roberta-base](https://huggingface.co/xlm-roberta-base) language model; includes tokenization and sentence splitting; outputs [UD](https://universaldependencies.org/)-features as [morphological categories](https://cl.ut.ee/ressursid/morfo-systeemid/index.php?lang=en). 
* Container and interface code


## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/elg_estspacysuntax:2022.06.29
```
Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading [source code](https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git) 

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git gitlab_docker_elg_estspacysuntax
```

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estspacysuntax
docker build -t tilluteenused/elg_estspacysuntax:2022.06.29 .
```

## Starting the container <a name="Starting_the_container"></a>

```commandline
docker run -p 8000:8000 tilluteenused/elg_estspacysuntax:2022.06.29
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

```json
{
  "type":"text",
  "content": string, /* The text of the request */
}
```

## Response json

```json
{
  "response":{
    "type":"texts",
    "texts":[             /* array of tokens */
      {
        "content": string /* token */
        "features":
            {
                "i": int,
                "is_sent_end": boolean, 
                "is_sent_start": boolean, 
                "pos": string, 
                "morph": string, /* Morphological info */
                "head": int,
                "dep": string, /* {"ROOT"|"obj"|"punct"|"prt"|...}
            }
      }
    ]
  }
}
```
## Usage example

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text", "content":"Mees peeti kinni. Sarved&Sõrad"}' http://127.0.0.1:5000/process
```

```json
HTTP/1.1 200 OK
Server: gunicorn
Date: Wed, 29 Jun 2022 14:52:17 GMT
Connection: close
Content-Type: application/json
Content-Length: 760

{"response":{"type":"texts","texts":[{"content":"Mees","features":{"i":0,"is_sent_end":false,"is_sent_start":true,"pos":92,"morph":"Case=Nom|Number=Sing","head":"1","dep":"obj"}},{"content":"peeti","features":{"i":1,"is_sent_end":false,"is_sent_start":false,"pos":100,"morph":"Mood=Ind|Tense=Past|VerbForm=Fin|Voice=Pass","head":"1","dep":"ROOT"}},{"content":"kinni","features":{"i":2,"is_sent_end":false,"is_sent_start":false,"pos":86,"morph":"","head":"1","dep":"compound:prt"}},{"content":".","features":{"i":3,"is_sent_end":true,"is_sent_start":false,"pos":97,"morph":"","head":"1","dep":"punct"}},{"content":"Sarved&S\u00f5rad","features":{"i":4,"is_sent_end":true,"is_sent_start":true,"pos":96,"morph":"Case=Nom|Number=Sing","head":"4","dep":"ROOT"}}]}}
```

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
